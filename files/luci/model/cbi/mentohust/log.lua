local fs = require "nixio.fs"

local tabname = {translate("MentoHUST Settings"), translate("MentoHUST LOG")};
local tabmenu = {
    luci.dispatcher.build_nodeurl("admin", "network", "mentohust", "general"),
	luci.dispatcher.build_nodeurl("admin", "network", "mentohust", "log"),
};
local isact = {false, true};
local tabcount = #tabname;

local f = SimpleForm("mentohust")

f.istabform = true
f.tabcount = tabcount
f.tabname = tabname;
f.tabmenu = tabmenu;
f.isact = isact;

local o = f:field(TextValue, "mentohust_log", translate("MentoHUST LOG"), translate("Log file:/tmp/mentohust.log"))

o.rows = 24

function o.cfgvalue(self, section)
	return fs.readfile("/tmp/mentohust.log")
end

function o.write(self, section, value)
	require("luci.sys").call('cat /dev/null > /tmp/mentohust.log 2>/dev/null')
end

f.submit = translate("Clear log")
f.reset = false

return f