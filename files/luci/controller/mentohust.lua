module("luci.controller.mentohust", package.seeall)

function index()
    if not nixio.fs.access("/etc/config/mentohust") then
        return
    end

	page = entry({"admin", "network", "mentohust"},alias("admin", "network", "mentohust", "general"),_("MentoHUST"), 30)
	page.i18n = "mentohust"
	page.dependent = true

	page = entry({"admin", "network", "mentohust", "general"}, cbi("mentohust/general"), _("MentoHUST Settings"), 10)
	page.i18n = "mentohust"
	page.hidden = true
	page.leaf = true

    page = entry({"admin", "network", "mentohust", "log"}, cbi("mentohust/log"), _("MentoHUST LOG"), 20)
	page.i18n = "mentohust"
	page.hidden = true
	page.leaf = true

end
